package mk.andrej.zadachaGitMavenCollectionFramework;

public class App {

	public static void main(String[] args) {
		Crud c = new Crud();

		c.add(new Student(2, "Nikola", "Nikolovski"));
		c.add(new Student(3, "Kire", "Kirev"));
		c.add(new Student(1, "Andrej", "Popovski"));
		c.read();

		System.out.println("\nUpdating the student that has the name Andrej to " + "a new student object\n");
		c.update("Andrej", new Student(5, "Andrejchoho", "Popovski"));
		c.read();

		System.out.println("\nDeleting id 2 and 5\n");
		c.delete(2);
		c.delete(5);
		c.read();

	}

}
