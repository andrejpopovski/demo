package mk.andrej.zadachaGitMavenCollectionFramework;

public interface CrudInt {
	public void add(Student student);

	public void update(String name, Student student);

	public void delete(int id);

	public void read();

}
