package mk.andrej.zadachaGitMavenCollectionFramework;

import java.util.ArrayList;
import java.util.List;

public class Crud implements CrudInt {

	private List<List<Student>> listOflists = new ArrayList<>();
	List<Student> studentList;

	@Override
	public void add(Student student) {
		studentList = new ArrayList<Student>();
		studentList.add(student);
		listOflists.add(studentList);
	}

	@Override
	public void update(String name, Student student) {
		for (List<Student> list : listOflists) {
			for (Student studenta : list) {
				if (studenta.getFirstName() == name) {
					studenta.setFirstName(student.getFirstName());
					studenta.setLastName(student.getLastName());
					studenta.setId(student.getId());
				}
			}

		}

	}

	@Override
	public void delete(int id) {

		for (List<Student> list : listOflists) {
			for (Student studenta : list) {
				if (studenta.getId() == id) {
					list.remove(studenta);

					break;
				}

			}

		}
	}

	@Override
	public void read() {
		System.out.println(listOflists);
	}

}
